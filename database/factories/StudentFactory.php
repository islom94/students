<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->lastName,
        'patronymic' => $faker->name,
        'birthday' => '1994-09-01',
        'math' => rand(3, 5),
        'physics' => rand(3, 5),
        'chemistry' => rand(3, 5),
        'history' => rand(3, 5),
        'literature' => rand(3, 5)
    ];
});
