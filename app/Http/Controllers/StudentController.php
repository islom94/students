<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Http\Resources\StudentCollection;

class StudentController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name'       => 'required|max:20',
            'surname'    => 'required|max:20',
            'patronymic' => 'required|max:20',
            'birthday'   => 'required|date',
            'math'       => 'required|integer|min:3|max:5',
            'physics'    => 'required|integer|min:3|max:5',
            'chemistry'  => 'required|integer|min:3|max:5',
            'history'    => 'required|integer|min:3|max:5',
            'literature' => 'required|integer|min:3|max:5',
        ]);

        $student = new Student();

        $student->fill($request->all());

        $student->save();

        return response()->json('successfully added');
    }

    public function index()
    {
        $students = Student::all();
        return new StudentCollection($students);
    }

    public function edit($id)
    {
        $student = Student::find($id);
        return response()->json($student);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name'       => 'required|max:20',
            'surname'    => 'required|max:20',
            'patronymic' => 'required|max:20',
            'birthday'   => 'required|date',
            'math'       => 'required|integer|min:3|max:5',
            'physics'    => 'required|integer|min:3|max:5',
            'chemistry'  => 'required|integer|min:3|max:5',
            'history'    => 'required|integer|min:3|max:5',
            'literature' => 'required|integer|min:3|max:5',
        ]);

        $student = Student::find($id);

        $student->fill($request->all());

        $student->save();

        return response()->json('successfully updated');
    }

    public function delete($id)
    {
        Student::destroy($id);

        return response()->json('successfully deleted');
    }
}
