<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
      'name', 'surname', 'patronymic', 'birthday',
        'math', 'physics', 'chemistry', 'history',
        'literature'
    ];
}
