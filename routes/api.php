<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('student')->group(function() {
    Route::post('create', 'StudentController@store');
    Route::get('edit/{id}', 'StudentController@edit');
    Route::post('update/{id}', 'StudentController@update');
    Route::delete('delete/{id}', 'StudentController@delete');
});

Route::get('/students', 'StudentController@index');
